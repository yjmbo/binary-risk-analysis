# Binary Risk Analysis

A simple open-ended tool to guide a conversation around risk assessment around the 'Risk = Impact x Likelihood' model

## How to use the tool

* Open the HTML file 'binary-ra.html' in a web browser with CSS and JavaScript support.
* Enter your own values for The Asset, The Threat, The Context
* Rate the 10 questions with yes or no answers
* Copy/paste the resulting page into any other document, to preserve your answers.

## Inspiration

[https://binary.protect.io/](https://binary.protect.io/) from Ben Sapiro was the starting point;
his tool (presented at SecTor 2011) was for a paper-based approach,
which is not a bad way of doing things at all.

I wasn't completely comfortable with some of the wording/phraseology and wanted try simplifying
it further; and while I originally made this as a spreadsheet (OpenOffice Calc) I felt
that it would be more useful as a simple web page, to remove error in the final calculations
and help people concentrate on the wording of their particular situation.

## License

Because Ben's tool was CC-licensed, so is this. [Attribution-ShareAlike 3.0 New Zealand (CC BY-SA 3.0 NZ)](https://creativecommons.org/licenses/by-sa/3.0/nz/)
